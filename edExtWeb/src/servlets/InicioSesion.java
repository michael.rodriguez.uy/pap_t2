package servlets;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.Icon;
import javax.xml.rpc.ServiceException;

import excepciones.UsuarioNoExisteException;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;
import publicadores.DtUsuario;

/**
 * Servlet implementation class InicioSesion
 */
@WebServlet("/InicioSesion")
public class InicioSesion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InicioSesion() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
	}
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String inputLogin = request.getParameter("nick");
		String password = request.getParameter("password");
		
		RequestDispatcher rd;
		boolean conectado=false;
		try {
			conectado = loginNickname(inputLogin, password);
			
		} catch (UsuarioNoExisteException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (conectado) {
			HttpSession sesion = request.getSession(conectado);
			publicadores.DtUsuario u = null;
			try {
				u = seleccionarDtUsuario(inputLogin);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(u == null) {
				String username = null;
				try {
					username = emailToNick(inputLogin);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					u=seleccionarDtUsuario(username);					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			}
			sesion.setAttribute("conectado", true);
			sesion.setAttribute("esDocente", u.isEsDocente());			
			sesion.setAttribute("nombre", u.getNombre());
			sesion.setAttribute("apellido", u.getApellido());
			sesion.setAttribute("imagenPerfil", u.getImagen());
			try {
				sesion.setAttribute("nombreImagen", rutaAnombreDeImagen(u.getImagen()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sesion.setAttribute("nickName", u.getNickName());
			sesion.setAttribute("email", u.getCorreo());
			/* para pasar la fecha como string */
			String pattern = "dd/MM/yyyy";
			DateFormat df = new SimpleDateFormat(pattern);
			Calendar fechaNac = u.getFechaNacimiento();
			// Esto ya no va mas con calendar -> String fechaString = df.format(fechaNac);		
			sesion.setAttribute("fechaNacimiento", fechaNac);
			//if(!u.isEsDocente()) {sesion.setAttribute("listaEDC", u.getInscripcionesDt());}
			try {
				sesion.setAttribute("listaUsuario", listarUsuario());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				sesion.setAttribute("usuariosSeguidos", usuariosSeguidos(u.getNickName()));
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				sesion.setAttribute("usuario", seleccionarUsuario(u.getNickName()));
			} catch (RemoteException | ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			request.setAttribute("mensaje", "Login correcto.");
			rd = request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
			
		} else {			
			String mensaje = "Login o contrase&ntilde;a incorrecta.";
			request.setAttribute("mensajeError", mensaje);
			request.setAttribute("hayError", true);
			
			rd = request.getRequestDispatcher("iniciarSesion.jsp");
            rd.forward(request,response);

		}

	}
	
	//OPERACIONES CONSUMIDAS	
	public boolean loginNickname(String inputLogin, String password) throws Exception {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return port.loginNickname(inputLogin, password);
	}
	public publicadores.DtUsuario seleccionarDtUsuario(String inputLogin) throws Exception {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return port.seleccionarDtUsuario(inputLogin);
	}
	public String emailToNick(String inputLogin) throws Exception {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return port.emailToNick(inputLogin);
	}
	public String rutaAnombreDeImagen(String imagen) throws Exception {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return port.rutaAnombreDeImagen(imagen);
	}	
	
	public String[] listarUsuario() throws ServiceException, RemoteException {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return port.listarUsuarios();
	}
	
	public String[] usuariosSeguidos(String nickSeguido) throws ServiceException, RemoteException {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return port.usuariosSeguidos(nickSeguido);
	}
	
	public DtUsuario seleccionarUsuario(String nickName) throws ServiceException, RemoteException {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return port.seleccionarDtUsuario(nickName);
	}
	
	/*
	Falta implementar
	public Date getFechaNacimiento() throws Exception {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return port.getFechaNacimiento();
	}
	
	getInscripcionesDt
	
	listarUsuarios
	
	*/	
	

	

}

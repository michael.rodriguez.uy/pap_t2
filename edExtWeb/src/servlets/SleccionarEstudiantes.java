package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import publicadores.DtEdicion;
import publicadores.DtInscripcionEdc;
import publicadores.DtUsuario;
import publicadores.EstadoInscripcionEdc;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class SleccionarEstudiantes
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
@WebServlet("/SleccionarEstudiantes")
public class SleccionarEstudiantes extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SleccionarEstudiantes() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		Fabrica fabrica = Fabrica.getInstancia();
		IControladorInstProg iconIP	= fabrica.getIControladorIP();
		
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		try {
			ControladorPublish port = cps.getControladorPublishPort();
			DtEdicion dte=ContinuarCurso.getUltimaedicion();
			String edicion=dte.getNombre();
			DtInscripcionEdc[] inscripciones=ContinuarCurso.getInscripcioness();
			String[] values = (String[])request.getParameterValues("aceptados");
			ArrayList<DtInscripcionEdc> listaAceptados = new ArrayList<>();
			EstadoInscripcionEdc estado;
			if(values!=null) {
				for(DtInscripcionEdc dti: inscripciones) {
					//System.out.println("Seleccionar Estudiante For1 "+ dti.getEstudiante().getCorreo());
					boolean yaaceptado = dti.getEstado()==EstadoInscripcionEdc.Aceptado;
					boolean seleccionado=false;
					for(String s:values) {
						//System.out.println("Seleccionar Estudiante For2 "+ s +" "+ dti.getEstudiante().getNickName());					
						if(dti.getEstudiante().getNickName().contentEquals(s)) {
							System.out.println("Seleccionar Estudiante For2 "+ s +" "+ dti.getEstudiante().getCorreo());
							seleccionado=true;
						}
					}
					if(seleccionado ||yaaceptado ) {
						estado=EstadoInscripcionEdc.Aceptado;
					}else {
						estado=EstadoInscripcionEdc.Rechazado;
					}
					DtUsuario usr = new DtUsuario(dti.getEstudiante().getNickName(),"", "", "", null,false,"",null);
					DtEdicion edc= new DtEdicion(0,null,null,null,edicion,edicion);//(edicion, null, null,0,null,null,"");
					DtInscripcionEdc dtedc = new DtInscripcionEdc(edc,usr,null,estado);//(edc, usr, new Date(), estado);
					listaAceptados.add(dtedc);
				}
	            Object[] o = listaAceptados.toArray();
	            DtInscripcionEdc[] miarray = new DtInscripcionEdc[o.length];
	            for (int i = 0; i < o.length; i++) {
	            	miarray[i] = (DtInscripcionEdc) o[i];
	            }	            
				port.setearInscripciones(ContinuarCurso.getInstituto(), ContinuarCurso.getCurso(), edicion, miarray);
				//  for(DtInscripcionEdc dtedc:listaAceptados ) System.out.println("Lista de Aceptados: "+ dtedc.getEstudiante().getNickName() + " " +dtedc.getEstado().toString() );
				response.getWriter().append("Se aceptaron los estudiantes seleccionados.");
			}else {
				response.getWriter().append("Ocurrio un error.");
			}
		
		//System.out.println("Seleccionar Estudiantes: "+  ContinuarCurso.getInstituto());
		}catch(ServiceException e) {
			System.out.println("Service Exception");
		}	
	}

}

package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class CargarDatosIniciales
 */
@WebServlet("/CargarDatosIniciales")
public class CargarDatosIniciales extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CargarDatosIniciales() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());		
		RequestDispatcher rd;
		
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		try {
			ControladorPublish port 	= cps.getControladorPublishPort();
			String[] listCat		 	=  port.listarCategorias();	
			List<String> listCategorias = new ArrayList<String>(Arrays.asList(listCat));	
			String[] listInstitutos 	=  port.listarInstitutosArray();
			String[] listaDoc 		= port.listarDocentes();
			List<String> listaDocentes = new ArrayList<String>(Arrays.asList(listaDoc));	
			String[] listProgramas 		= port.listarProgramas();
	        
	        HttpSession sesion;
	               
	        sesion = request.getSession(false);
	        sesion.setAttribute("inicio", "vuelve"); 
	        sesion.setAttribute("listInstitutos", listInstitutos);
	        sesion.setAttribute("listCategorias", listCategorias);
	        sesion.setAttribute("listProgramas", listProgramas);
	        
			/*Lista de Docentes*/		
			sesion.setAttribute("listDocente", listaDocentes);
			

			rd = request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
	
		}catch(ServiceException e) {
			String text = "Error en  getControladorPublishPort CargarDatosIniciales.java";
			response.setContentType("text/plain");
			response.getWriter().write(text);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

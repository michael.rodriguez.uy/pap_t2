package servlets;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Arrays;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import logica.Usuario;
import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;
import publicadores.DtUsuario;
import excepciones.InscripcionEdicionRepetida;


@WebServlet("/InscripcionEdicion")
public class InscripcionEdicion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InscripcionEdicion() {
        super();
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Fabrica fabrica = Fabrica.getInstancia();
		//IControladorInstProg iconins = fabrica.getIControladorIP();
		RequestDispatcher rd;
		
		System.out.println("Inscripcion Test: me cree los controladores");
		
		String nombreEdicion = request.getParameter("edicion") ;
		String nickEstudiante = request.getParameter("usuario");

		//IControladorUsuario iconu = fabrica.getIControladorU();
		HttpSession sesion = request.getSession(true);
		try {
			altaInscripcionEdc(nombreEdicion, nickEstudiante);
			String mensaje = "Se ha registrado a la edicion" + nombreEdicion; 
			//Usuario u = iconu.seleccionarUsuario((String) sesion.getAttribute("nickName"));
			publicadores.Usuario u = seleccionarUsuario((String) sesion.getAttribute("nickName"));
			System.out.println("Agarro el usuario con el nick de sesion : "+(String) sesion.getAttribute("nickName"));
			// debemos volver a setear esta variable de sesion
			//sesion.setAttribute("listaEDC", u.getInscripcionesDt());
			sesion.setAttribute("listaEDC", Arrays.asList(u.getInscripciones()));
			request.setAttribute("mensajeOK", mensaje);
			rd = request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			String mensaje = "Usted ya est&aacute; inscripto a " + nombreEdicion; 
			request.setAttribute("mensajeError", mensaje);
			rd = request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
		}
	}
	public void altaInscripcionEdc(String nombreEdicion,String nickEstudiante) throws Exception{
		ControladorPublishService cps= new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		try {
			port.altaInscripcionEdc(nombreEdicion,nickEstudiante);
		}
		catch(RemoteException e){
			e.printStackTrace();
		}
	}
	public publicadores.Usuario seleccionarUsuario(String nick) throws ServiceException, RemoteException {
		ControladorPublishService cps= new ControladorPublishServiceLocator();
		ControladorPublish port = cps.getControladorPublishPort();
		return port.seleccionarUsuario(nick);
	}

}

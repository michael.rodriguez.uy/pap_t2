package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.service.spi.ServiceException;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;
import publicadores.DtPrograma;

/**
 * Servlet implementation class Prueba
 */
@WebServlet("/Programas")
public class Programas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Programas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		ControladorPublish port;
		try {
			port = cps.getControladorPublishPort();
			String progSel= (String) request.getParameter("progselecc");
			System.out.println("Servlet.Cursos.doGet " +progSel );
			System.out.println("Mi programa es: " + progSel);
			if(progSel!=null) {
				
				//String[] listaProgramas = iconip.listarProgramas();
				DtPrograma programa = port.verPrograma(progSel);
				request.setAttribute("programa", programa);
				
				
				String[] cursos = port.cursosDePrograma();
				//List<Curso> cursos = programa.getCursos();
				request.setAttribute("cursos", cursos);
				
				String[] categorias = port.listarCategoriasPF(progSel);
				//String[] categorias = programa.getCategorias();
				request.setAttribute("categorias", categorias);
			}
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
		
		RequestDispatcher rd;
		rd = request.getRequestDispatcher("programas.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
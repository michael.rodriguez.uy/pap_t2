package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;


@WebServlet("/Edicion")
public class Edicion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Edicion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String nombreInstituto	= (String) request.getParameter("i");
		String nombreEdicion 	= (String) request.getParameter("e");
		String nombreCurso	 	= (String) request.getParameter("c");
		HttpSession sesion;         
        sesion = request.getSession(false);
        sesion.setAttribute("status", "none");
        
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		try {
			ControladorPublish port 	= cps.getControladorPublishPort();
			RequestDispatcher rd;
	        System.out.println("nombreInstituto: " + nombreInstituto);
	        
			port.seleccionarInstituto(nombreInstituto);
			port.seleccionarCursoInstancia(nombreCurso);
	        
			if(!nombreInstituto.equals("null")) {
				System.out.println("-" + nombreEdicion + "-");			
				
				publicadores.DtEdicion dte= port.mostrarEdicion(nombreEdicion);
				System.out.println("DTE servlet: " + dte.getFechaI() );
				
				publicadores.DtInscripcionEdc[] ldtiArray = port.inscrpcionesAceptadas(nombreInstituto, nombreCurso, nombreEdicion);
				ArrayList<publicadores.DtInscripcionEdc> ldti = new ArrayList<publicadores.DtInscripcionEdc>(Arrays.asList(ldtiArray));
				String [] nombres= conseguirNickEstudiantes(ldti);
			
				request.setAttribute("dtEdicion", dte);	
				request.setAttribute("nickaceptados", nombres);	
				rd = request.getRequestDispatcher("mostrarEdicion.jsp");
				rd.forward(request, response);
			}else {
				String instDelCurso= port.institutoDeCurso(nombreCurso);
				if(instDelCurso!=null) {
					publicadores.DtEdicion dte= port.mostrarEdicion(nombreEdicion);
					publicadores.DtInscripcionEdc[] ldtiArray = port.inscrpcionesAceptadas(instDelCurso, nombreCurso, nombreEdicion);
					ArrayList<publicadores.DtInscripcionEdc> ldti = new ArrayList<publicadores.DtInscripcionEdc>(Arrays.asList(ldtiArray));
					String [] nombres= conseguirNickEstudiantes(ldti);
					request.setAttribute("dtEdicion", dte);	
					request.setAttribute("nickaceptados", nombres);	
					rd = request.getRequestDispatcher("mostrarEdicion.jsp");
					rd.forward(request, response);
				}else {
					rd = request.getRequestDispatcher("CargarDatosIniciales");
				}
				
				//rd = request.getRequestDispatcher("CargarDatosIniciales");
				
			}
		}catch(ServiceException e) {
			String text = "Error en  getControladorPublishPort CargarDatosIniciales.java";
			response.setContentType("text/plain");
			response.getWriter().write(text);
		}
		
		
	}

	public String [] conseguirNickEstudiantes(ArrayList<publicadores.DtInscripcionEdc> listaDTinsc) {
		int arraysize = listaDTinsc.size();
		String [] retorno = new String [arraysize];
		if(arraysize>0) {
			for(int i=0; i < arraysize; i++) {
			System.out.println(listaDTinsc.get(i).getEstudiante().getNickName());
			retorno[i]=listaDTinsc.get(i).getEstudiante().getNickName();
			}
		}
		return retorno;
		
	}

}

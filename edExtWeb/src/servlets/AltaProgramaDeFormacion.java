package servlets;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorPublish;
import publicadores.ControladorPublishService;
import publicadores.ControladorPublishServiceLocator;

/**
 * Servlet implementation class AltaProgramaDeFormacion
 */

@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
@WebServlet("/AltaProgramaDeFormacion")
public class AltaProgramaDeFormacion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AltaProgramaDeFormacion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String nombre = (String)request.getParameter("nomPrograma") ; //System.out.println(nombre);
		String desc = (String)request.getParameter("DescripcionPrograma") ;
		
		ControladorPublishService cps = new ControladorPublishServiceLocator();
		try {
			ControladorPublish port 	= cps.getControladorPublishPort();

			String instituto = (String)request.getParameter("instituto"); System.out.println(instituto);
			port.seleccionarInstituto(instituto);
			
			Part archivo = request.getPart("imagenProg");
			String foto;
			if(archivo.getSize()>0) {
				 foto =  archivo.getSubmittedFileName();				
				String fotoRuta = System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "cursos"+ File.separator + foto;				
				//foto=fotoRuta; //preguntar porque estaba eso, a mi no me anda asi
				archivo.write(fotoRuta);
			}else {
				String fotoRuta = System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "cursos"+ File.separator + "cursodefault.jpg";
				foto="cursodefault.jpg";
			}
			
			//System.out.println("doc: " + arregloDocentes);
			String fechaInicio	= (String)request.getParameter("inputFechaIniProg");
			//System.out.println("fechaInicio: " + fechaInicio);
			String fechaFin	= (String)request.getParameter("inputFechaFinProg");
			//System.out.println("fechaFin: " + fechaFin);
			
			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
			Date fechaInicioFormat= null;		
			try {
				fechaInicioFormat= sdf.parse(fechaInicio);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Date fechaFinFormat= null;		
			try {
				fechaFinFormat= sdf.parse(fechaFin);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
				
			archivo.write(foto);
			Calendar fechaInicioG= dateToCalendar(fechaInicioFormat);
			Calendar fechaFinG= dateToCalendar(fechaFinFormat);
			
			port.altaPrograma(nombre, desc, fechaInicioG, fechaFinG, foto);
			String text = "ok?Se creo el Programa de Formaci&oacute;n:  " + nombre;
			response.setContentType("text/plain");
			response.getWriter().write(text);
				
			
		}catch(ServiceException e) {
			String text = "Error en  getControladorPublishPort AltaProgramaDeFormacion.java";
			response.setContentType("text/plain");
			response.getWriter().write(text);
		}	
	}
			
	private Calendar dateToCalendar(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;

    }
		
}


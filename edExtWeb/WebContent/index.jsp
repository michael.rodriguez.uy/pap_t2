<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="es">
<head>
<%@include file="header.jsp"%>
</head>
<body>
	<%
		if (request.getAttribute("inicio") == null && request.getAttribute("inicio") != "vuelve") {
			RequestDispatcher rd;
			request.setAttribute("inicio", "si");
			rd = request.getRequestDispatcher("CargarDatosIniciales");
			rd.forward(request, response);
		}
	%>
	<%
		HttpSession sesion = request.getSession();
		if (sesion.getAttribute("conectado") == null) {
	%><%@include file="navbarSinSession.jsp"%>
	<%
		} else {
	%><%@include file="navbarSession.jsp"%>
	<%
		}
	%>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 p0">
				<%
					//si ls respuesta de mi servlet es diferente a vacio
					if (request.getAttribute("mensajeOK") != null) {
						//tendr�a que mandar a imprimir por medio del alert de abajo el mensaje de acceso correcto o incorrecto, pero no me lo muestra
				%>
				<div class="alert alert-success mb-2 text-center" role="alert">
					<%=request.getAttribute("mensajeOK")%>
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<%
					} else if (request.getAttribute("mensajeError") != null) {
				%>
				<div class="alert alert-danger mb-2 text-center" role="alert">
					<%=request.getAttribute("mensajeError")%>
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<%
					}
				%>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto ml-auto text-center">
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Institutos </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<%
							String[] listInstitutos = (String[]) sesion.getAttribute("listInstitutos");

							if (listInstitutos != null) {
								for (int i = 0; i < listInstitutos.length; i++) {
									out.println("<a class='dropdown-item' href='Institutos?inselecc=" + listInstitutos[i] + "'>"
											+ listInstitutos[i] + "</a>");
								}
							} else {
								out.println("<a class='dropdown-item'> No hay datos cargados </a>");
							}
						%>



					</div></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Categor&iacute;as </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<%
							ArrayList<String> listCategorias;
							listCategorias = (ArrayList<String>) session.getAttribute("listCategorias");

							if (listCategorias.size() > 0) {
								for (int counter = 0; counter < listCategorias.size(); counter++) {
									out.println("<a class='dropdown-item' href='Institutos?catselecc=" + listCategorias.get(counter)
											+ "'>" + listCategorias.get(counter) + "</a>");
								}
							} else {
								out.println("<a class='dropdown-item'> No hay datos cargados </a>");
							}
						%>
					</div></li>
					
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Programas </a> 
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<%
						
							String[] listProgramas = (String[]) sesion.getAttribute("listProgramas");
							
							if (listProgramas.length > 0) {
								for (int i = 0; i < listProgramas.length; i++) {
									out.println("<a class='dropdown-item' href='Programas?progselecc=" + listProgramas[i] + "'>"
											+ listProgramas[i] + "</a>");
								}
							} else {
								out.println("<a class='dropdown-item'> No hay datos cargados </a>");
							}
						%>
					</div>
				</li>
				<li>
					<div class="d-xl-none d-lg-none d-md-block">
			    		<a class="nav-link d-inline black" href="consultaUsuario.jsp"><%=sesion.getAttribute("nombre")%> <%=sesion.getAttribute("apellido")%>
			    			<img src="imagenes/<%=sesion.getAttribute("nombreImagen")%>" class='img-usuario rounded-circle' alt='Imagen Perfil'> 															
			    		</a>
		    		</div>
				</li>	
				<li>
					<div class="d-xl-none d-lg-none d-md-block">		    		
						<a href='LogOut' class=" nav-link d-inline blue">
				    		Salir
			    		</a>
		    		</div>
				</li>
			</ul>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12 p0">
				<div id="carouselExampleControls" class="carousel slide"
					data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img
								src="https://www.usal.es/files/glazed-builder/portal-formacion.jpg?fid=64434"
								class="d-block w-100">
							<div class="carousel-caption d-none d-md-block">
								<h3>
									Estrategia Institucional de Digitalizaci&oacute;n<br>De La
									Docencia Para El Curso 2020-21
								</h3>
							</div>
						</div>
						<div class="carousel-item">
							<img
								src="https://www.usal.es/files/glazed-builder/protocolocovidestudiantes-homeusal.jpg?fid=64401"
								class="d-block w-100">
							<div class="carousel-caption d-none d-md-block">
								<h3>
									Protocolo COVID-19<br>Para Estudiantes
								</h3>
							</div>
						</div>
						<div class="carousel-item">
							<img
								src="https://www.usal.es/files/glazed-builder/outdoc-homeusal.jpg?fid=64499"
								class="d-block w-100">
							<div class="carousel-caption d-none d-md-block">
								<h3>
									Proyecto Europeo De Empleabilidad<br>Para Doctornados
								</h3>
							</div>
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleControls"
						role="button" data-slide="prev"> <span
						class="carousel-control-prev-icon" aria-hidden="true"></span> <span
						class="sr-only">Previous</span>
					</a> <a class="carousel-control-next" href="#carouselExampleControls"
						role="button" data-slide="next"> <span
						class="carousel-control-next-icon" aria-hidden="true"></span> <span
						class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row p0">
			<div class="col-4 p0">
				<img
					src="https://image.freepik.com/foto-gratis/joven-estudiante-mascara-quirurgica-pie-detras-edificio-universitario-vidrio_101969-1244.jpg"
					class="img-fluid">
			</div>
			<div class="col-4 p0">
				<img
					src="https://image.freepik.com/foto-gratis/reunion-remota-hombre-trabajando-casa-coronavirus-o-cuarentena-covid-19-concepto-oficina-remota_155003-12357.jpg"
					class="img-fluid">
			</div>
			<div class="col-4 p0">
				<img
					src="https://image.freepik.com/foto-gratis/manos-guantes-medicos-hisopo-covid-19-tubo-ensayo-tomar-muestra-paciente-proceso-protocolo-prueba-adn-pcr-prueba-laboratorio-hisopo-nasal-laboratorio-hospital_180532-783.jpg"
					class="img-fluid">
			</div>
			<div class="col-12 p0">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.5328694759182!2d-56.16876118491068!3d-34.9181705803792!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f81987d10c86d%3A0xf302eed9b9f6410a!2sFacultad%20de%20Ingenier%C3%ADa!5e0!3m2!1ses-419!2suy!4v1603579862742!5m2!1ses-419!2suy" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			</div>			
		</div>

	</div>

	<%@include file="footer.jsp"%>

	<script>
		document.addEventListener("DOMContentLoaded", function(event) {
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					var data = xhr.responseText;
					//alert(data);
				}
			}
			xhr.open('GET', 'CargarDatosIniciales', true);
			xhr.send(null);
		});
		//funcion para mostrar en registrar los institutos cuando es docente.
		function habilitarInstitutos() {
			if (document.getElementById('institutosMostrar').style.display === "block") {
				document.getElementById("institutosMostrar").style.display = "none";
			} else {
				document.getElementById("institutosMostrar").style.display = "block";
			}

		}
	</script>

</body>
</html>
<%@page import="publicadores.DtCurso"%>
<%@page import="publicadores.DtEdicion"%>
<%@page import="publicadores.DtPrograma"%>
<%@page import="java.util.ArrayList"%>
<%//@page import="logica.Programa"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="header.jsp"%>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<%
		HttpSession sesion = request.getSession();
		if (sesion.getAttribute("conectado") == null) {
	%><%@include file="navbarSinSession.jsp"%>
	<%
		} else {
	%><%@include file="navbarSession.jsp"%>
	<%
		}

		//List<String> listaCategoria = request.getAttribute("listCategorias");
	%>
	<%
		if (request.getAttribute("inicio") == null && request.getAttribute("inicio") != "vuelve") {
			RequestDispatcher rd;
			request.setAttribute("inicio", "si");
			rd = request.getRequestDispatcher("Institutos");
			rd.forward(request, response);
		}
	%>
	<nav class="navbar navbar-expand-lg navbar-light bg-light d-block d-sm-block d-md-none d-lg-none d-xl-none">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto ml-auto text-center">
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Institutos </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<%
							String[] listInstitutos2 = (String[]) sesion.getAttribute("listInstitutos");

							if (listInstitutos2 != null) {
								for (int i = 0; i < listInstitutos2.length; i++) {
									out.println("<a class='dropdown-item' href='Institutos?inselecc=" + listInstitutos2[i] + "'>"
											+ listInstitutos2[i] + "</a>");
								}
							} else {
								out.println("<a class='dropdown-item'> No hay datos cargados </a>");
							}
						%>



					</div></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Categor&iacute;as </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<%
							ArrayList<String> listCategorias2;
							listCategorias2 = (ArrayList<String>) session.getAttribute("listCategorias");

							if (listCategorias2.size() > 0) {
								for (int counter = 0; counter < listCategorias2.size(); counter++) {
									out.println("<a class='dropdown-item' href='Institutos?catselecc=" + listCategorias2.get(counter)
											+ "'>" + listCategorias2.get(counter) + "</a>");
								}
							} else {
								out.println("<a class='dropdown-item'> No hay datos cargados </a>");
							}
						%>
					</div>
				</li>
				<li>
					<div class="d-xl-none d-lg-none d-md-block">
			    		<a class="nav-link d-inline black" href="consultaUsuario.jsp"><%=sesion.getAttribute("nombre")%> <%=sesion.getAttribute("apellido")%>
			    			<img src="imagenes/<%=sesion.getAttribute("nombreImagen")%>" class='img-usuario rounded-circle' alt='Imagen Perfil'> 															
			    		</a>
		    		</div>
				</li>				
				<li>
					<div class="d-xl-none d-lg-none d-md-block">		    		
						<a href='LogOut' class=" nav-link d-inline blue">
				    		Salir
			    		</a>
		    		</div>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container-fluid mt-5">
		<div class="row">
			<div class="d-none d-sm-none d-md-block d-lg-block d-xl-block col-md-3 col-lg-3 col-xl-3">
				<%@include file="menuIzq.jsp"%>
			</div>
			<%
				DtEdicion[][] listBidtEdiciones = (DtEdicion[][]) request.getAttribute("listBidtEdiciones");
				int topesEdi[] = (int[]) request.getAttribute("topesEdi");
				DtPrograma[][] listBidtProgramas = (DtPrograma[][]) request.getAttribute("listBidtProgramas");
				int topesProg[] = (int[]) request.getAttribute("topesProg");
				DtCurso[] listdtCursos = (DtCurso[]) request.getAttribute("listdtCursos");//if(listdtCursos!=null){for(int i=0; i < listdtCursos.length;  i++){ System.out.println(listdtCursos[i].getFoto());} }

				String nombreInstituto = (String) request.getAttribute("nombreInstituto");
				String catSel = (String) request.getParameter("catselecc");
				System.out.println(nombreInstituto);
			%>

			<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
				<div id="mensajes"></div>
				<div class="card">
					<%
						boolean docente = false;
						if (sesion.getAttribute("conectado") != null) {
							docente = (boolean) sesion.getAttribute("esDocente");
						} else {
							docente = false;
						}
					%>
					<div class="card-body titulo">
						<%
							if (nombreInstituto != null) {
								out.println("<h2 class='d-inline'>" + nombreInstituto + "</h2>");
							} else {
								out.println("<h2 class='d-inline'>" + catSel + "</h2>");
							}

							if (nombreInstituto != null && docente) {
								out.println(
										"<button type='button' class='d-inline btn btn-success float-right' data-toggle='modal' data-target='#exampleModal'>Agregar Curso</button>");
								
						%>
						<button type="button"
							class="btn btn-success float-right d-inline mr-2" data-toggle="modal"
							data-target="#agregarModalPrograma">Agregar Programa</button>
						
						<!-- MODAL AGREGAR CURSO -->
						<div class="modal fade" id="exampleModal" tabindex="-1"
							aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Agregar
											Curso</h5>
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">

										<form enctype="multipart/form-data"
											onsubmit="submitForm(); return false;" id="form">
											<input type="hidden" id="instituto" name="instituto"
												value="<%out.print(nombreInstituto);%>"> <label
												for="uname">Nombre Del Curso:</label> <input type="text"
												class="form-control" id="nomCurso"
												placeholder="Ingrese nombre del Curso" name="nomCurso"
												required>
											<div class="valid-feedback">Valid.</div>
											<div class="invalid-feedback">Please fill out this
												field.</div>
											<label for="desc">Descripcion:</label> <input type="text"
												class="form-control" id="desCurso" placeholder="Descripcion"
												name="desc" required>
											<div class="valid-feedback">Valid.</div>
											<div class="invalid-feedback">Please fill out this
												field.</div>


											<div class="row">
												<div class="col">
													<label for="dur">Duracion:</label> <input type="text"
														class="form-control" id="nomCurso" placeholder="Duracion"
														name="dur" required>
													<div class="valid-feedback">Valid.</div>
													<div class="invalid-feedback">Please fill out this
														field.</div>
												</div>
												<div class="col">
													<label for="horas">Cantidad de Horas:</label> <input
														type="number" class="form-control" id="nomCurso"
														placeholder="Cantidad de Horas" name="horas" required>
													<div class="valid-feedback">Valid.</div>
													<div class="invalid-feedback">Please fill out this
														field.</div>
												</div>
											</div>
											<div class="row">
												<div class="col">
													<label for="creditos">Creditos:</label> <input
														type="number" class="form-control" id="nomCurso"
														placeholder="Creditos" name="creditos" required>
													<div class="valid-feedback">Valid.</div>
													<div class="invalid-feedback">Please fill out this
														field.</div>
												</div>
												<div class="col">
													<label for="url">url:</label> <input type="text"
														class="form-control" id="nomCurso" placeholder="url"
														name="url" required>
													<div class="valid-feedback">Valid.</div>
													<div class="invalid-feedback">Please fill out this
														field.</div>
												</div>
											</div>
											<div class="col">
												<label for=imagenCurso>Imagen: </label>
												<div class="form-group">

													<input type="file" name="imagenCurso" id="imagenCurso">
												</div>
											</div>
											<div class="form-group">

												<label for="exampleFormControlSelect2">Previas</label> <select
													multiple class="form-control"
													id="exampleFormControlSelect2" name="previas">
													<%
														if (listdtCursos != null) {
																for (DtCurso p : listdtCursos) {
																	out.println("<option>" + p.getNombre() + "</option>");
																}
															}
													%>
												</select>
											</div>
											<div class="form-group">
												<%
													ArrayList<String> listCategoriass = (ArrayList<String>) session.getAttribute("listCategorias");
												%>
												<label for="exampleFormControlSelect2">Categorias</label> <select
													multiple class="form-control"
													id="exampleFormControlSelect2" name="programas">
													<%
														for (String cat : listCategoriass) {
																out.println("<option>" + cat + "</option>");
															}
													%>>
												</select>
											</div>											
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
											<button type="submit" class="btn btn-primary">Crear Curso</button>
										</form>
									</div>									
								</div>
							</div>
						</div>

						<!--  FIN MODAL AGREGAR CURSO -->

						<%
							}
						%>
					</div>
				</div>
			
									<!-- MODAL AGREGAR programa -->
						<div class="modal fade" id="agregarModalPrograma" tabindex="-1"
							aria-labelledby="agregarModalPrograma" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="agregarModalPrograma">Agregar
											Programa de Formacion</h5>
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">

										<form enctype="multipart/form-data"
											onsubmit="submitFormProg(); return false;" id="formProg">
											<input type="hidden" id="instituto" name="instituto"
												value="<%out.print(nombreInstituto);%>"> <label
												for="uname">Nombre del Programa:</label> <input type="text"
												class="form-control" id="nomPrograma"
												placeholder="Ingrese nombre del Programa" name="nomPrograma"
												required>
											<div class="valid-feedback">Valid.</div>
											<div class="invalid-feedback">Please fill out this
												field.</div>
											<label for="desc">Descripcion:</label> <input type="text"
												class="form-control" id="DescripcionPrograma" placeholder="Descripcion Programa"
												name="DescripcionPrograma" required>
											<div class="valid-feedback">Valid.</div>
											<div class="invalid-feedback">Please fill out this
												field.</div>

											<div class="col">
												<label for=imagenCurso>Imagen</label>
												<div class="form-group">
													<input type="file" name="imagenProg" id="imagenProg">
												</div>
											</div>
											<div class="form-row">
													<div class="col">
														<div class="form-group">
															<label for="inputFechaIniProg">Fecha de Inicio</label> <input
																type="date" class="form-control" id="inputFechaIniProg"
																name="inputFechaIniProg" value="" min="1900-01-01"
																max="2021-12-31" required>
														</div>
													</div>
													<div class="col">
														<div class="form-group">
															<label for="inputFechaFinProg">Fecha de Fin</label> <input
																type="date" class="form-control" id="inputFechaFinProg"
																name="inputFechaFinProg" value="" min="1900-01-01"
																max="2021-12-31" required>
														</div>
													</div>
												</div>
												<button type="button" class="btn btn-secondary"
											data-dismiss="modal">Cerrar</button>
												<button type="submit" class="btn btn-primary">Crear Programa</button>
										</form>
									</div>									
								</div>
							</div>
						</div>

						<!--  FIN MODAL AGREGAR programa -->
			
			
			
			
				<div class="card">
					<div class="card-body">
						<div class="row">
							<%
								String[] listaCursos = (String[]) request.getAttribute("nomCurss");
								if (listdtCursos != null) {
									for (int i = 0; i < listdtCursos.length; i++) {
							%>
							<div class="col-md-6 col-sm-6 col-lg-4">
								<div class="card">
									<%
										if (listdtCursos[i].getFoto() != null) {
													out.println("<img src='" + "imagenes/cursos/" + listdtCursos[i].getFoto()
															+ "' class='card-img-top img-fluid' alt='Imagen not found' >");
												} else {
													out.println(
															"<img src='imagenes/cursos/cursodefault.jpg' class='card-img-top' alt='Imagen not found'>");
												}
									%>
									<div class="card-body">
										<%
											out.println("<h5 class='card-title'><a  href='Cursos?inselecc=" + nombreInstituto + "&curselecc="
															+ listdtCursos[i].getNombre() + "'>" + listdtCursos[i].getNombre() + "</a></h5>");
													out.println("<p class='card-text'>" + listdtCursos[i].getDescripcion() + "</p>");

													out.println("<a class='btn btn-block btn-sm btn-outline-secondary' href='Cursos?inselecc="
															+ nombreInstituto + "&curselecc=" + listdtCursos[i].getNombre() + "'> Ir a Curso</a>");
										%>
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item subtitulo-card-edicion">Ediciones</li>
										<%
											for (int j = 0; j < topesEdi[i]; j++) {
														if (true) { // condicion era nombreInstituto != null te largaba una alarma
															out.println("<li class='list-group-item'><a href='Edicion?i=" + nombreInstituto + "&amp;c="
																	+ listdtCursos[i].getNombre() + "&amp;e=" + listBidtEdiciones[i][j].getNombre()
																	+ "'>" + listBidtEdiciones[i][j].getNombre() + "</a></li>");
														} else {
															out.println("<li class='list-group-item'><a href='javascript:funcion()'>" + listBidtEdiciones[i][j].getNombre() + "</a></li>");
										%> 
										<script>
											//'Cursos?inselecc=" + nombreInstituto + "&curselecc=" + listdtCursos[i].getNombre() + "'
											function funcion() {
												alert('Ingrese al curso para ver la edicion');
											}
										</script>
										<%
											}
													} // ediciones 
													if (topesEdi[i] == 0) {
														out.println("<li class='list-group-item'>No hay Ediciones cargadas</li>");
													}
										%>
										<li class="list-group-item subtitulo-card-programa">Programas</li>
										<%
																				
											for (int j = 0; j < topesProg[i]; j++) {
														out.println("<li class='list-group-item'><a  href='#'>" + listBidtProgramas[i][j].getNombre()
																+ "</a></li>");
													}
													if (topesProg[i] == 0) {
														out.println("<li class='list-group-item'>No hay Programas cargados</li>");
													}
										%>
									</ul>

								</div>
							</div>
							<%
								} // for
								} else {
									out.println(
											"<div class='col-sm-12'><div class='alert alert-danger' role='alert'>No hay Cursos cargados para el instituto "
													+ nombreInstituto + "</div>");
								}
							%>
						</div>

					</div>
				</div>
			</div>
		</div>
						
		
		
		<%@include file="footer.jsp"%>
		<script>
			function _(id) {
				return document.getElementById(id);
			}
			function submitForm() {
				var formData = new FormData(_("form"));
				var ajax = new XMLHttpRequest();
				ajax.open("POST", "AltaCurso", true);

		    	$('#exampleModal').modal('hide')
				ajax.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						if (this.responseText == "Exito!") {
							document.getElementById("mensajes").innerHTML = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> "
									+ "El Curso fue dado de alta."
									+ "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
						} else {
							document.getElementById("mensajes").innerHTML = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> "
									+ "Ya existe un Curso con ese nombre."
									+ "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
						}
						document.getElementById("form").reset();
					}
					;
				}
				ajax.send(formData);
			}
			
		</script>
		
		<script>
		function _(id) {
			return document.getElementById(id);
		}
		function submitFormProg() {
			var formData = new FormData(_("formProg"));
			var ajax = new XMLHttpRequest();
			ajax.open("POST", "AltaProgramaDeFormacion", true);

			ajax.onreadystatechange = function() {
			    if (this.readyState == 4 && this.status == 200) {
			    	var dev = this.responseText.replace("Served at: /edExtWeb", "");
			    	var res = dev.split("?");
			    	$('#agregarModalPrograma').modal('hide')
			    	if(res[0] == "ok"){
			    		document.getElementById("mensajes").innerHTML = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> " + res[1] + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
			    	} else {
			    		document.getElementById("mensajes").innerHTML = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> " + res[1] + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
			    	}
			    		    				   
			    };
			}
			ajax.send(formData);
		}
		</script>
</body>
</html>
function getSelectedCheckboxValues(docente) {
    const checkboxes = document.querySelectorAll(`input[name="${docente}"]:checked`);
    let values = [];
    checkboxes.forEach((checkbox) => {
        values.push(checkbox.value);
    });
    return values;
}

const btn = document.querySelector('#crearEdicion');

btn.addEventListener('click', (event) => {
    let arregloDocentes = getSelectedCheckboxValues('docente');
    var nombreEdicion 	= document.getElementById("inputNombre").value;
    var fechaInicio		= document.getElementById("inputFechaIni").value;
    var fechaFin		= document.getElementById("inputFechaFin").value;
    var fechaPub		= document.getElementById("inputFechaPub").value;       
    var cupo			= document.getElementById("inputCupo").value;
    var img				= document.getElementById("inputImagenEdicion").value;    
    var url 			= window.location.search;
    var res = url.split("&");
    
    var idInstituto = res[0].replace("?inselecc=", "");
    idInstituto = idInstituto.replace("%20", " ");
    
    var idCurso= res[1].replace("curselecc=", "");
    
    idCurso = idCurso.replace("%20", " ");
	
  	//var parametros = "idInstituto=" + idInstituto + "&idCurso=" + idCurso + "&arregloDocentes=" + arregloDocentes + "&nombreEdicion=" + nombreEdicion + "&fechaInicio=" + fechaInicio + "&fechaFin=" + fechaFin + "&fechaPub=" + fechaPub + "&cupo=" + cupo + "&img=" + img;	    		
	
	var ajax=new XMLHttpRequest();
	
	var fd = new FormData();    
	fd.append( 'img', document.getElementById("inputImagenEdicion").files[0] );
	fd.append('nombreEdicion', nombreEdicion);
	fd.append('arregloDocentes', arregloDocentes);
	fd.append('fechaInicio', fechaInicio);
	fd.append('fechaFin', fechaFin);
	fd.append('fechaPub', fechaPub);
	fd.append('cupo', cupo);
	fd.append('url', url);
	fd.append('idInstituto', idInstituto);
	fd.append('idCurso', idCurso);
			
	
	ajax.open("POST","AltaEdicion", true);
	ajax.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	    	
	    	var dev = this.responseText.replace("Served at: /edExtWeb", "");
	    	var res = dev.split("?");
	    	$('#agregarEdicion').modal('hide')
	    	if(res[0] == "ok"){
	    		document.getElementById("mensajes").innerHTML = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> " + res[1] + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
	    	} else {
	    		document.getElementById("mensajes").innerHTML = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Mensaje: </strong> " + res[1] + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
	    	}
	    	
    			    				   
	    };
	}
	ajax.send(fd);
    
});
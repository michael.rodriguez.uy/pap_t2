<nav class="navbar navbar-expand-md navbar-dark bg-dark">
		  <a class="navbar-brand" href="index.jsp">
		    <img src="img/icono.png" width="30" height="30" alt="Logo">
		  </a>		  
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
	
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    		<ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
	   		    <form class="form-inline ml-2" action="BuscarCurso" method="post">
			      <input class="form-control mr-sm-2" type="search" placeholder="" aria-label="Buscar">
			      <button class="btn btn-outline-success my-2 my-sm-0" disabled>Buscar</button>
			    </form>
	    	  </li>
		    </ul>
			<ul class="navbar-nav ml-auto">
		      <li class="nav-item active">
		    		<a class="nav-link" href="iniciarSesion.jsp">Iniciar Sesion</a>
		    	</li>
		    	<li class="nav-item active">
		    	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#registrarse">
				  Registrarse
				</button>
		    	</li>
		    </ul>		    
		  </div>
		</nav>
		<!-- Modal -->
		<div class="modal fade" id="registrarse" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="staticBackdropLabel">Registrarse</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <form  class="form" enctype="multipart/form-data"  action="AltaUsuario" method="post">
		          <div class="form-row">
				    <div class="col">
      				  <div class="form-group">
					    <label for="inputNickName">NickName</label>
					    <input type="text" class="form-control" id="inputNickName" name="nickName" required>
					  </div>
				    </div>
				    <div class="col">
      				  <div class="form-group">
					    <label for="inputEmail">Email</label>
					    <input type="email" class="form-control" id="inputEmail" name="email" required>
					  </div>
				    </div>
				  </div>
				  
  		          <div class="form-row">
				    <div class="col">
      				  <div class="form-group">
					    <label for="inputNombre">Nombre</label>
					    <input type="text" class="form-control" id="inputNombre" name="nombre" required>
					  </div>
				    </div>
				    <div class="col">
      				  <div class="form-group">
					    <label for="inputApellido">Apellido</label>
					    <input type="text" class="form-control" id="inputApellido" name="apellido" required>
					  </div>
				    </div>
				  </div>
				  
				  <div class="form-row">
				    <div class="col">
      				  <div class="form-group">
					    <label for=inputImagenPerfil>Imagen de Perfil: </label>
					    <input type="file" name="imagenPerfil" id="inputImagenPerfil" >
					  </div>
				    </div>
				    <div class="col">
      				  <div class="form-group">
					    <label for="inputFechaNac">Fecha de Nacimiento</label>
					    <input type="date" class="form-control" id="inputFechaNac" name="inputFechaNac" value="" min="1900-01-01" max="2021-12-31" required>
					  </div>
				    </div>
				  </div>
				  
  				  <div class="form-row">
				    <div class="col">
      				  <div class="form-group">
					    <label for=inputPasword>Password</label>
					    <input type="password" class="form-control" id="inputPasword" name="password" required>
					  </div>
				    </div>
				    <div class="col">
      				  <div class="form-group">
					    <label for="inputPasword2">Repetir password</label>
					    <input type="password" class="form-control" id="inputPasword2" name="password2" required>
					  </div>
				    </div>
				  </div>
				  
  				  <div class="form-row">
				    <div class="col">
      				  <div class="form-group">      				  
					    <label class="form-check-label" for="inputEsDocente">Es Docente: </label>
					    <input type="checkbox" class="form-check-input" name= "esDocente" id="inputEsDocente" onclick="habilitarInstitutos()">					        					
					  </div>
				    </div>
				  </div>	
  				  <div class="form-row" id="institutosMostrar" style="display:none">
				    <div class="col">
      				  <div class="form-group">      				  
					    <label for="selectInstituto">Instituto</label>
					    <select class="form-control" id="selectInstituto" name="instituto" required>
					    <% 
						    String[] listInstitutos = (String[]) sesion.getAttribute("listInstitutos");
							
							if( listInstitutos != null){
								for (int i=0; i < listInstitutos.length;  i++){
									out.println("<option>" + listInstitutos[i] + "</option>");
								}	
							} else {out.println("<li class='list-group-item'> No hay datos cargados </li>");}
								
					    %>
					    </select>					        					
					  </div>
				    </div>
				  </div>				  				  
				  <button type="button" class="btn btn-secondary" data-dismiss="modal" > Cerrar</button>
				  <button type="submit" class="btn btn-primary">Registrarse</button>
				</form>
		      </div>
		    </div>
		  </div>
		</div>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.sql.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="servlets.SeguirUsuario"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="datatypes.DtInscripcionEdc"%>
<%@page import="datatypes.DtEdicion"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="es">
<head>
<%@include file="header.jsp"%>
</head>
<body>
	<%
		HttpSession sesion = request.getSession();
		if (sesion.getAttribute("conectado") == null) {
	%><%@include file="navbarSinSession.jsp"%>
	<%
		} else {
	%><%@include file="navbarSession.jsp"%>
	<%
		}

		//List<String> listaCategoria = request.getAttribute("listCategorias");
	%>

	<div class="container-fluid mt-5">
		<div class="row">
			<div class="col-3">
				<%@include file="menuIzq.jsp"%>
			</div>
			<div class="col-9">
				<div class="container-fluid">
					<div class="row">
						<div class="media col-12">
							<img src="imagenes/<%=sesion.getAttribute("nombreImagen")%>"
								class='img-usuario-consulta rounded-circle' alt='Imagen Perfil'>
							<%--
				    			String img = (String) sesion.getAttribute("imagenPerfil");
				    			// hasta no solucionar que se devuelva solo el nombre de la imagen comento esto:
				    			if(img != null){	
				    				out.println("<img src='"+img+"' class='img-usuario rounded-circle' alt='Imagen Perfil'>");		    		
			    				} else {
			    					out.println("<img src='img/usuario/defecto.jpg' class='mr-3 img-fluid' alt='Imagen Perfil'>");
			    					
			    				}
				    		--%>
							<div class="media-body">
								<%
									String nombreCompleto = (String) sesion.getAttribute("nombre") + " "
											+ (String) sesion.getAttribute("apellido");
									out.println("<h5 class='mt-0'>" + nombreCompleto + "</h5>");
									String nickName = (String) sesion.getAttribute("nickName");
									String email = (String) sesion.getAttribute("email");
									out.println("<p>" + nickName + " / " + email + "</p>");
								%>
							</div>
						</div>
						<div class="col-12 mt-4">
							<!-- SE SEPARA SI ES DOCENTE O ESTUDIANTE -->
							<%
								boolean docente = (boolean) sesion.getAttribute("esDocente");
								if (docente) {
							%>
							<!-- ES DOCENTE -->
							<nav>
								<div class="nav nav-tabs" id="nav-tab" role="tablist">
									<a class="nav-link active" id="nav-generalDocente-tab"
										data-toggle="tab" href="#nav-generalDocente" role="tab"
										aria-controls="nav-generalDocente" aria-selected="true">GENERAL</a>
									<a class="nav-link" id="nav-edicion-cursos-tab"
										data-toggle="tab" href="#nav-edicion-cursos" role="tab"
										aria-controls="nav-profile" aria-selected="false">EDICION
										DE CURSOS</a>
									<a
										class="nav-link" id="nav-seguidos-tab" data-toggle="tab"
										href="#nav-seguidos" role="tab" aria-controls="nav-contact"
										aria-selected="false">SEGUIDOS</a>
								</div>
							</nav>
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="nav-generalDocente"
									role="tabpanel" aria-labelledby="nav-generalDocente-tab">
									<!-- INFO GENERAL -->
									<div class="card">
										<div class="card-body">
											<form action="ModificarUsuario" method="post">
												<%
												String nickname = (String) sesion.getAttribute("nickName");
												String nombre = (String) sesion.getAttribute("nombre");
												String apellido = (String) sesion.getAttribute("apellido");
												//email lo tengo de antes
												Calendar fecha = (Calendar)sesion.getAttribute("fechaNacimiento");	
													
													int dia = fecha.getTime().getDate();
													int mes = fecha.getTime().getMonth() + 1;
													int anio = fecha.getTime().getYear() + 1900;
													
													
													
													String f = dia + "/" + mes + "/" + anio;
													Calendar cal = Calendar.getInstance();
													SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
													cal.setTime(sdf.parse(f));
												
												%>
												<div class="form-row">
													<div class="col">
														<div class="form-group">
															<label for="inputNickName">NickName</label> <input
																type="text" class="form-control" id="inputNickName"
																name="nickName" readonly
																value="<%out.println(nickname);%>" required>
														</div>
													</div>
													<div class="col">
														<div class="form-group">
															<label for="inputEmail">Correo Electr&oacute;nico</label>
															<input type="email" class="form-control" id="inputEmail"
																readonly name="email" value="<%out.println(email);%>"
																required>
														</div>
													</div>
												</div>

												<div class="form-row">
													<div class="col">
														<div class="form-group">
															<label for="inputNombre">Nombre</label> <input
																type="text" class="form-control" id="inputNombre"
																name="nombre" value="<%out.println(nombre);%>">
														</div>
													</div>
													<div class="col">
														<div class="form-group">
															<label for="inputApellido">Apellido</label> <input
																type="text" class="form-control" id="inputApellido"
																name="apellido" value="<%out.println(apellido);%>">
														</div>
													</div>
												</div>

												<div class="form-row">
													<div class="col">
														<div class="form-group">
															<label for="inputFechaNac">Fecha de Nacimiento</label> <input
																type="text" class="form-control"
																onfocus="(this.type='date')" id="inputFechaNac"
																name="fechaNac"
																value="<%out.println(dia + "/" + mes + "/" + anio);%>"
																min="1900-01-01" max="2021-12-31">
														</div>
													</div>
												</div>
												<button type="submit" class="btn btn-primary">Modificar
													Datos</button>
												<%
													//si ls respuesta de mi servlet es diferente a vacio
														if (request.getAttribute("mensajeError") != null) {
															//tendr�a que mandar a imprimir por medio del alert de abajo el mensaje de acceso correcto o incorrecto, pero no me lo muestra
												%>
												<div class="alert alert-danger" role="alert">
													<%=request.getAttribute("mensajeError")%>
												</div>
												<%
													}
												%>
											</form>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="nav-edicion-cursos"
									role="tabpanel" aria-labelledby="nav-edicion-cursos-tab">
									<div class="card">
										<div class="card-body">
										No existen ediciones para este Docente
										</div>	
									</div>
								</div>
								<div class="tab-pane fade" id="nav-seguidos" role="tabpanel" aria-labelledby="nav-seguidos-tab">
							<div class="card">
								<div class="card-body">
									<ul class="list-group list-group">						  	 
								  		<%
								  		//String[] listUsu = (String[])sesion.getAttribute("listaUsuario");
								  		String[] listUsu = (String[])sesion.getAttribute("listaUsuario");
										if( listUsu != null){
											for (String nick: listUsu){
												%><form action="SeguirUsuario" method="post"><%
													
												if(!nick.equals(sesion.getAttribute("nickName"))){%>
													<input type="hidden" class="form-control" id="inputNickNameSeguidor" name="nickNameSeguidor" readonly value = "<%=sesion.getAttribute("nickName")%>" required>
													<input type="hidden" class="form-control" id="inputNickNameSeguido" name="nickNameSeguido" readonly value = "<%=nick%>" required>
													<%;
													boolean esta = false;
													if(sesion.getAttribute("usuariosSeguidos")!=null){
														List<publicadores.Usuario> usuariosSeg = (List<publicadores.Usuario>)sesion.getAttribute("usuariosSeguidos");
														for(publicadores.Usuario u: usuariosSeg){
															if(nick.equals(u.getNickName())){
																esta = true;
															}
															System.out.println("veamos que usuarios tenemos aqui: "+u.getNickName());
														}
													}
													if(esta){
														out.println("<li class='list-group-item'>"+nick +" <button type='submit' class='btn btn-primary btn-sm float-right' name='nick' >Dejar de seguir <svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-search' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z'/><path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z'/></svg></button></li>");
													}
													else{
														out.println("<li class='list-group-item'>"+nick +" <button type='submit' class='btn btn-primary btn-sm float-right' name='nick'>Seguir <svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-search' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z'/><path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z'/></svg></button></li>");
													}

												}
												%>
												</form>
												<% 
											}
											
										} else {out.println("<li class='list-group-item'> No hay ning&uacute;n usuario para seguir. </li>");}
								  		%>
									 
									</ul>		
							</div>
						</div>  
							</div>
							</div>
							<%
								} else {
							%>
							<!-- ES ESTUDIANTE -->
							<nav>
								<div class="nav nav-tabs" id="nav-tab" role="tablist">
									<a class="nav-link active" id="nav-generalEstudiante-tab"
										data-toggle="tab" href="#nav-generalEstudiante" role="tab"
										aria-controls="nav-generalEstudiante" aria-selected="true">GENERAL</a>
									<a class="nav-link" id="nav-cursos-tab" data-toggle="tab"
										href="#nav-cursos" role="tab" aria-controls="nav-profile"
										aria-selected="false">EDICION DE CURSOS</a> 
									<a
										class="nav-link" id="nav-seguidos-tab" data-toggle="tab"
										href="#nav-seguidos" role="tab" aria-controls="nav-contact"
										aria-selected="false">SEGUIDOS</a>
								</div>
							</nav>
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active"
									id="nav-generalEstudiante" role="tabpanel"
									aria-labelledby="nav-generalEstudiante-tab">
									<!-- INFO GENERAL -->
									<div class="col">
										<div class="card-body">
											<form action="ModificarUsuario" method="post">
												<%
													String nickname = (String) sesion.getAttribute("nickName");
													String nombre = (String) sesion.getAttribute("nombre");
													String apellido = (String) sesion.getAttribute("apellido");
													//email lo tengo de antes
													Calendar fecha = (Calendar)sesion.getAttribute("fechaNacimiento");	
													
													int dia = fecha.getTime().getDate();
													int mes = fecha.getTime().getMonth() + 1;
													int anio = fecha.getTime().getYear() + 1900;
													
													String f = dia + "/" + mes + "/" + anio;
													Calendar cal = Calendar.getInstance();
													SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
													cal.setTime(sdf.parse(f));
													
												%>
												<div class="form-row">
													<div class="col">
														<div class="form-group">
															<label for="inputNickName">NickName</label> <input
																type="text" class="form-control" id="inputNickName"
																name="nickName" readonly
																value="<%out.println(nickname);%>" required>
														</div>
													</div>
													<div class="col">
														<div class="form-group">
															<label for="inputEmail">Correo Electr&oacute;nico</label>
															<input type="email" class="form-control" id="inputEmail"
																readonly name="email" value="<%out.println(email);%>"
																required>
														</div>
													</div>
												</div>

												<div class="form-row">
													<div class="col">
														<div class="form-group">
															<label for="inputNombre">Nombre</label> <input
																type="text" class="form-control" id="inputNombre"
																name="nombre" value="<%out.println(nombre);%>">
														</div>
													</div>
													<div class="col">
														<div class="form-group">
															<label for="inputApellido">Apellido</label> <input
																type="text" class="form-control" id="inputApellido"
																name="apellido" value="<%out.println(apellido);%>">
														</div>
													</div>
												</div>

												<div class="form-row">
													<div class="col">
														<div class="form-group">
															<label for="inputFechaNac">Fecha de Nacimiento</label> <input
																type="text" class="form-control"
																onfocus="(this.type='date')" id="inputFechaNac"
																name="fechaNac"
																value="<%out.println(dia + "/" + mes + "/" + anio);//dia + "/" + mes + "/" + anio);%>"
																min="1900-01-01" max="2021-12-31">
														</div>
													</div>
												</div>
												<button type="submit" class="btn btn-primary">Modificar Datos</button>
												<%
													//si ls respuesta de mi servlet es diferente a vacio
														if (request.getAttribute("mensajeError") != null) {
															//tendr�a que mandar a imprimir por medio del alert de abajo el mensaje de acceso correcto o incorrecto, pero no me lo muestra
												%>
												<div class="alert alert-danger" role="alert">
													<%=request.getAttribute("mensajeError")%>
												</div>
												<%
													}
												%>
											</form>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="nav-cursos" role="tabpanel"
									aria-labelledby="nav-cursos-tab">
									<div class="card">
										<div class="card-body">
											<ul class="list-group list-group">
												<%
													if(sesion.getAttribute("listaEDC") != null){List<DtInscripcionEdc> liedc = (List<DtInscripcionEdc>) sesion.getAttribute("listaEDC");
														if (liedc.size() > 0) {
															for (int i = 0; i < liedc.size(); i++) {
																DtEdicion dte = liedc.get(i).getEdicion();
												%>
												<li class="list-group-item"><%=liedc.get(i).getFecha()%>
													- <%=liedc.get(i).getEdicion().getNombre()%> - <%=liedc.get(i).getEstado()%>
													<button type="button"
														class="btn btn-primary btn-sm float-right"
														data-toggle="modal" value="" data-target="#modal<%=i%>">
														Ver informaci&oacute;n
														<svg width="1em" height="1em" viewBox="0 0 16 16"
															class="bi bi-search" fill="currentColor"
															xmlns="http://www.w3.org/2000/svg">
															<path fill-rule="evenodd"
																d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z" />
										    <path fill-rule="evenodd"
																d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z" />
										   </svg>
													</button></li>
												<!-- MODAL DE LA EDICION SELECCIONADA -->
												<div class="modal fade" id="modal<%=i%>" tabindex="-1"
													aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><%=liedc.get(i).getEdicion().getNombre()%></h5>
																<button type="button" class="close" data-dismiss="modal"
																	aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">

																<div class="form-row">
																	<div class="col-12">
																		<div class="form-group">
																			<label for="inputApellido">Fecha de Inicio :
																				<%=liedc.get(i).getEdicion().getFechaI()%></label> <label
																				for="inputApellido">Fecha de Fin: <%=liedc.get(i).getEdicion().getFechaF()%></label>
																		</div>
																																<hr>
																	<ul class="list-group">
																		<li class="list-group-item subtitulo-card-edicion">Docentes</li>
																		<%
																			if (dte.getListaDocentes().size() == 0) {
																				out.println("<li class='list-group-item'>");
																				out.println("<p>No hay Docentes en esta Edici&oacute;n</p>");
																				out.println("</li>");
																			} else {
																				for (int a = 0; a < dte.getListaDocentes().size(); a++) {
																					out.println("<li class='list-group-item'>");
																					out.println(dte.getListaDocentes().get(a).getNombre() + " "
																					+ dte.getListaDocentes().get(a).getApellido());
																					out.println("</li>");
																				}
																			}
																		%>
																	</ul>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<%
													}
														} else {
															out.println(
																	"<li class='list-group-item'> Usted no esta inscripto a ning&uacute;na edicion de curso. </li>");
														}
													}
												%>
											</ul>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="nav-programas" role="tabpanel"
								aria-labelledby="nav-programas-tab">
							<div class="card">
								<div class="card-body">Programas</div>
							</div>
						</div>
												
						<div class="tab-pane fade" id="nav-seguidos" role="tabpanel" aria-labelledby="nav-seguidos-tab">
							<div class="card">
								<div class="card-body">
									<ul class="list-group list-group">						  	 
								  		<%
								  		String[] listUsu = (String[])sesion.getAttribute("listaUsuario");
										if( listUsu != null){
											for (String nick: listUsu){
												%><form action="SeguirUsuario" method="post"><%
													
												if(!nick.equals(sesion.getAttribute("nickName"))){%>
													<input type="hidden" class="form-control" id="inputNickNameSeguidor" name="nickNameSeguidor" readonly value = "<%=sesion.getAttribute("nickName")%>" required>
													<input type="hidden" class="form-control" id="inputNickNameSeguido" name="nickNameSeguido" readonly value = "<%=nick%>" required>
													<%;
													boolean esta = false;
													if(sesion.getAttribute("usuariosSeguidos")!=null){
														List<String> usuariosSeg = (List<String>)sesion.getAttribute("usuariosSeguidos");
														for(String u: usuariosSeg){
															if(nick.equals(u)){
																esta = true;
															}
														}
													}
													if(esta){
														out.println("<li class='list-group-item'>"+nick +" <button type='submit' class='btn btn-primary btn-sm float-right' name='nick' >Dejar de seguir <svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-search' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z'/><path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z'/></svg></button></li>");
													}
													else{
														out.println("<li class='list-group-item'>"+nick +" <button type='submit' class='btn btn-primary btn-sm float-right' name='nick'>Seguir <svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-search' fill='currentColor' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' d='M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z'/><path fill-rule='evenodd' d='M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z'/></svg></button></li>");
													}

												}
												%>
												</form>
												<% 
											}
											
										} else {out.println("<li class='list-group-item'> No hay ning&uacute;n usuario para seguir. </li>");}
								  		%>
									 
									</ul>		
							</div>
						</div>  
							</div>
							</div>
							<%
								}
							%>
							<!-- TERMINA ES ESTUDIANTE -->
								  
						</div>			
					</div>
				</div>
			</div>
		</div>
	</div>
<%@include file="footer.jsp"%>
</body>
</html>
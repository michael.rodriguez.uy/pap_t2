<%@page import="publicadores.DtCurso"%>
<%@page import="publicadores.DtEdicion"%>
<%@page import="publicadores.DtInscripcionEdc"%>
<%@page import="publicadores.EstadoInscripcionEdc"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.*,java.text.*"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<%@include file="header.jsp"%>
<meta charset="ISO-8859-1">
<title>Mostrar Edicion</title>
</head>
<body>

	<%
		HttpSession sesion = request.getSession();
		if (sesion.getAttribute("conectado") == null) {
	%><%@include file="navbarSinSession.jsp"%>
	<%
		} else {
	%><%@include file="navbarSession.jsp"%>


	<%
		}
		if (request.getAttribute("inicio") == null && request.getAttribute("inicio") != "vuelve") {
			RequestDispatcher rd;
			request.setAttribute("inicio", "si");
			rd = request.getRequestDispatcher("Edicion");
			rd.forward(request, response);
		}
		publicadores.DtEdicion dte = (DtEdicion) request.getAttribute("dtEdicion");
		
	
		System.out.println("DTE jsp: " + dte.getFechaI() );

		
		boolean docente = false;
		if (sesion.getAttribute("conectado") != null) {
			docente = (boolean) sesion.getAttribute("esDocente");
		} else {
			docente = false;
		}
	%>

	<nav class="navbar navbar-expand-lg navbar-light bg-light d-block d-sm-block d-md-none d-lg-none d-xl-none">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto ml-auto text-center">
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Institutos </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<%
							String[] listInstitutos2 = (String[]) sesion.getAttribute("listInstitutos");

							if (listInstitutos2 != null) {
								for (int i = 0; i < listInstitutos2.length; i++) {
									out.println("<a class='dropdown-item' href='Institutos?inselecc=" + listInstitutos2[i] + "'>"
											+ listInstitutos2[i] + "</a>");
								}
							} else {
								out.println("<a class='dropdown-item'> No hay datos cargados </a>");
							}
						%>



					</div></li>
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> Categor&iacute;as </a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<%
							ArrayList<String> listCategorias2;
							listCategorias2 = (ArrayList<String>) session.getAttribute("listCategorias");

							if (listCategorias2.size() > 0) {
								for (int counter = 0; counter < listCategorias2.size(); counter++) {
									out.println("<a class='dropdown-item' href='Institutos?catselecc=" + listCategorias2.get(counter)
											+ "'>" + listCategorias2.get(counter) + "</a>");
								}
							} else {
								out.println("<a class='dropdown-item'> No hay datos cargados </a>");
							}
						%>
					</div>
				</li>
				<li>
					<div class="d-xl-none d-lg-none d-md-block">
			    		<a class="nav-link d-inline black" href="consultaUsuario.jsp"><%=sesion.getAttribute("nombre")%> <%=sesion.getAttribute("apellido")%>
			    			<img src="imagenes/<%=sesion.getAttribute("nombreImagen")%>" class='img-usuario rounded-circle' alt='Imagen Perfil'> 															
			    		</a>
		    		</div>
				</li>				
				<li>
					<div class="d-xl-none d-lg-none d-md-block">		    		
						<a href='LogOut' class=" nav-link d-inline blue">
				    		Salir
			    		</a>
		    		</div>
				</li>
			</ul>
		</div>
	</nav>
		
	<div class="container-fluid mt-5">
		<div class="row">
			<div class="d-none d-sm-none d-md-block d-lg-block d-xl-block col-md-3 col-lg-3 col-xl-3">
				<%@include file="menuIzq.jsp"%>
			</div>
			<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
				<%
					//si ls respuesta de mi servlet es diferente a vacio
					if (request.getAttribute("mensajeOK") != null) {
						//tendr�a que mandar a imprimir por medio del alert de abajo el mensaje de acceso correcto o incorrecto, pero no me lo muestra
				%>
				<div class="alert alert-success mb-2" role="alert">
					<%=request.getAttribute("mensajeOK")%>
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<%
					} else if (request.getAttribute("mensajeError") != null) {
				%>
				<div class="alert alert-danger mb-2" role="alert">
					<%=request.getAttribute("mensajeError")%>
					<button type="button" class="close" data-dismiss="alert"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<%
					}
				%>

				<div id="mensajes"></div>
				<div class="card">
					<div class="card-body titulo">
						<%
							out.println("<h2 class='d-inline'>" + dte.getNombre() + "</h2>");
							if (sesion.getAttribute("conectado") != null) {
								if (!docente) {
						%>
						<button type="button" class="btn btn-info d'inline float-right"
							data-toggle="modal" data-target="#modalinscripcion">
							Inscribirme a Edici&oacute;n</button>
						<%
							} // if !docente 
							} // if sesion conectado
						%>
					</div>
					<div class="modal fade" id="modalinscripcion" tabindex="-1"
						role="dialog" aria-labelledby="modalinscripciontexto"
						aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="modalinscripciontexto"><%=dte.getNombre()%></h5>
									<button type="button" class="close" data-dismiss="modal"
										aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									Confirma su inscripci&oacute;n a
									<%=dte.getNombre()%>
								</div>
								<div class="modal-footer">
									<form class="form" method="post" action="InscripcionEdicion">
										<input type="hidden" name="usuario"
											value="<%=sesion.getAttribute("nickName")%>" /> <input
											type="hidden" name="edicion" value="<%=dte.getNombre()%>" />
										<button type="button" class="btn btn-secondary"
											data-dismiss="modal">Cancelar</button>
										<button type="submit" class="btn btn-primary">
											Aceptar</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<%
									if (dte.getImagen() != null) {
										out.println("<img src='" + "imagenes/cursos/" + dte.getImagen()
												+ "' class='img-curso img-fluid' alt=\"...\" >");
									} else {
										out.println(
												"<img src='imagenes/cursos/cursodefault.jpg' class='img-curso img-fluid' alt='Image not found' >");
									}
								%>
							</div>
							<div class="col-sm-8">
								<p>
									<span class="font-weight-bold">Fecha de Inicio: </span>
									<%
										//out.println(dte.getFechaI().getTime().toLocaleString());
									%>
								</p>
								<p>
									<span class="font-weight-bold">Fecha de Fin:</span>
									<%
										//out.println(dte.getFechaF().getTime().toLocaleString());
									%>
								</p>
								<p>
									<span class="font-weight-bold">Fecha de
										Publicaci&oacute;n:</span>
									<%
									
										//out.println(dte.getFechaPub().getTime().toLocaleString());
									%>
								</p>
								<p>
									<span class="font-weight-bold">Cupo:</span>
									<%
										out.println(dte.getCupo());
									%>
								</p>
							</div>
							<div class="col-12">
								<hr>
								<ul class="list-group">
									<li class="list-group-item subtitulo-card-edicion">Docentes</li>
									<%
									/*
										if (dte.getListaDocentes().size() == 0) {
											out.println("<li class='list-group-item'>");
											out.println("<p>No hay Docentes en esta Edici&oacute;n</p>");
											out.println("</li>");
										} else {
											for (int i = 0; i < dte.getListaDocentes().size(); i++) {

												out.println("<li class='list-group-item'>");
												out.println(dte.getListaDocentes().get(i).getNombre() + " "
														+ dte.getListaDocentes().get(i).getApellido());
												out.println("</li>");
											}
										}
									*/
									%>

								</ul>
								<hr>
								<% if (docente) { %>
								<ul class="list-group">
									<li class="list-group-item subtitulo-card-aceptados">Estudiantes
										Aceptados</li>
									<%
										String[] aceptados = (String[]) request.getAttribute("nickaceptados");
										if (aceptados.length == 0) {
											out.println("<li class='list-group-item'><p>No hay Estudiantes aceptados</p></li>");
										}
										if (aceptados.length > 0) {
											for (String a : aceptados) {
												out.println("<li class='list-group-item'><a class='dropdown-item'>" + a + "</a></li>");
											}
										}
									%>
								</ul>
								<%} %>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@include file="footer.jsp"%>
</body>
</html>